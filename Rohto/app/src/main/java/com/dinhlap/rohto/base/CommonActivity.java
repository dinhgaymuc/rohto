package com.dinhlap.rohto.base;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.model.Account;


public class CommonActivity extends BaseActivity {
    private Fragment fragment;


    @Override
    protected int getLayoutId() {
        return 0 ;
    }

    @Override
    protected Class getViewModel() {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS); }
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorBlack));
        }
        try {
            Class<?> clazz = (Class) getIntent().getSerializableExtra("FRAGMENT");
            if (clazz != null) {
                fragment = (Fragment)clazz.newInstance();
            } else {
                String className = this.getIntent().getStringExtra("FRAGMENT_NAME");
                fragment = (Fragment)((Fragment)Class.forName(className).newInstance());
            }
            FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.framelayout_common, fragment);
            ft.commit();

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    public Fragment getFragment() {
        return this.fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

}
