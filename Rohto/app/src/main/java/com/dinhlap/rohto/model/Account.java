package com.dinhlap.rohto.model;

import android.util.Patterns;

import com.dinhlap.rohto.sharedPrefs.SharedPrefs;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account implements Serializable {
    String userName;
    String password;

    public Account() {
    }

    public Account(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public static String getNameAccount(){
        return SharedPrefs.getInstance().get(SharedPrefs.USER_NAME,String.class);
    }
    public static String getNamePassword(){
        return SharedPrefs.getInstance().get(SharedPrefs.PASSWORD,String.class);
    }

    public static boolean isEmailValid(CharSequence userName) {
        if(Patterns.EMAIL_ADDRESS.matcher(userName).matches()){
            return true;
        }else if(Patterns.PHONE.matcher(userName).matches()){
            return true;
        }
        return false;
    }
    public static boolean ispasswordlValid(String password) {
        return password.length() < 5 ? false : true ;
    }
}
