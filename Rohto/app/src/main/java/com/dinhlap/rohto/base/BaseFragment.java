package com.dinhlap.rohto.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;



public abstract class BaseFragment<V extends ViewDataBinding, VM extends ViewModel> extends Fragment {
    protected V binding;
    protected VM viewVM;

    protected abstract int getLayoutId();
    protected abstract Class getViewModel();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            super.onCreateView(inflater, container, savedInstanceState);
            this.binding = DataBindingUtil.inflate(inflater, this.getLayoutId(), container, false);
                viewVM = (VM) ViewModelProviders.of(this).get(getViewModel());
        } catch (Throwable var5) {
            var5.printStackTrace();
        }
        onCreateView();
        return this.binding.getRoot();
    }

    public abstract void onCreateView();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
