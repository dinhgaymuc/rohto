package com.dinhlap.rohto.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.base.BaseActivity;
import com.dinhlap.rohto.databinding.ActivityLoginBinding;
import com.dinhlap.rohto.model.Account;
import com.dinhlap.rohto.network.RunUi;
import com.dinhlap.rohto.utils.Constants;
import com.dinhlap.rohto.viewmodel.LoginVM;


public class Login extends BaseActivity<ActivityLoginBinding, LoginVM> implements View.OnClickListener, RunUi {
    private LoginVM loginVM;
    
    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected Class getViewModel() {
        return LoginVM.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.setUser(new Account(Account.getNameAccount(), Account.getNamePassword()));
        loginVM = viewVM;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_login:
                if (Account.isEmailValid(binding.editTextAccount.getText().toString().trim()) && (Account.ispasswordlValid(binding.editTextPassword.getText().toString().trim()))) {
                    loginVM.login(binding.editTextAccount.getText().toString().trim(), binding.editTextPassword.getText().toString().trim(), this);
                } else {
                    binding.tvWrongAccount.setText(getString(R.string.account_invalid));
                    binding.tvWrongAccount.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tv_register_account:
                Intent intent = new Intent(this, RegistrationActivity.class);
                intent.putExtra(Constants.KEYPAPE, Constants.REGISTRATION);
                startActivity(intent);
                break;
            case R.id.tv_forgot_password:
                Intent intent1 = new Intent(this, RegistrationActivity.class);
                intent1.putExtra(Constants.KEYPAPE, Constants.FORGOTPASSWORD);
                startActivity(intent1);
                break;


        }

    }

    @Override
    public void runSuccess(Object... objects) {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    @Override
    public void runFailure(String error) {
        if (error.equals("error")) {
            binding.tvWrongAccount.setText(getString(R.string.wrong_account));
            binding.tvWrongAccount.setVisibility(View.VISIBLE);
        } else {
            binding.tvWrongAccount.setText(error);
            binding.tvWrongAccount.setVisibility(View.VISIBLE);
        }


    }
}