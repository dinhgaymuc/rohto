package com.dinhlap.rohto.api;


import com.dinhlap.rohto.base.ResultData;
import com.dinhlap.rohto.model.Account;
import com.dinhlap.rohto.model.Token;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Philippe on 02/03/2018.
 */

public interface ApiService {

    @POST("login")
    Call<ResultData<Token>> login(@Body Account account);

}
