package com.dinhlap.rohto;



public class App extends android.app.Application {

    private static App mSelf;
    public static App self() {
        return mSelf;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSelf = this;
    }

}
