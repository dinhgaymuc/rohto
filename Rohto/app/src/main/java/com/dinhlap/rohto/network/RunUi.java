package com.dinhlap.rohto.network;

public interface RunUi {
    void runSuccess(Object... params);
    void runFailure(String error);
}