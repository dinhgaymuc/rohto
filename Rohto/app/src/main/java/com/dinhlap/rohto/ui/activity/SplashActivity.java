package com.dinhlap.rohto.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.base.BaseActivity;
import com.dinhlap.rohto.databinding.ActivitySplashBinding;
import com.dinhlap.rohto.model.Account;
import com.dinhlap.rohto.network.RunUi;
import com.dinhlap.rohto.viewmodel.LoginVM;

public class SplashActivity extends BaseActivity<ActivitySplashBinding, LoginVM> implements RunUi {


    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected Class getViewModel() {
        return LoginVM.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String userName = Account.getNameAccount();
        String password = Account.getNamePassword();
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!userName.equals("") && !password.equals("")) {
                    viewVM.login(userName, password, SplashActivity.this);
                } else {
                    startActivity(new Intent(getApplicationContext(), Login.class));
                    finish();
                }
            }
        }, 2000);
    }

    @Override
    public void runSuccess(Object... params) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void runFailure(String error) {
        startActivity(new Intent(this, Login.class));
        finish();
    }
}