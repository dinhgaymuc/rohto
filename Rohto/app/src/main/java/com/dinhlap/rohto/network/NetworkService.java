package com.dinhlap.rohto.network;

import com.dinhlap.rohto.BuildConfig;
import com.dinhlap.rohto.api.ApiService;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class NetworkService {

    public static volatile NetworkService mInstance = null;
    private static Retrofit retrofit;
    public static final String baseUrl = "http://45.118.144.92:8117/api/";

    public NetworkService() {
        OkHttpClient mClient  = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor( new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(mClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();
    }

    public static ApiService getService(){
        if (mInstance == null)
            mInstance = new NetworkService();
        return retrofit.create(ApiService.class);
    }

    public static  <T> T getService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }

}