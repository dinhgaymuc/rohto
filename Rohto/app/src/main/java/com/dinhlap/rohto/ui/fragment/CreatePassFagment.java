package com.dinhlap.rohto.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.base.BaseFragment;
import com.dinhlap.rohto.databinding.FragmentCreatePassBinding;
import com.dinhlap.rohto.dialog.DialogRegistrationConfirmation;
import com.dinhlap.rohto.ui.activity.Login;
import com.dinhlap.rohto.utils.Constants;
import com.dinhlap.rohto.viewmodel.CreatePassVM;

public class CreatePassFagment extends BaseFragment<FragmentCreatePassBinding,CreatePassVM> implements View.OnClickListener {
    private int keyPage;
    private DialogRegistrationConfirmation dialog;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_create_pass;
    }

    @Override
    protected Class getViewModel() {
        return CreatePassVM.class;
    }
    @Override
    public void onCreateView() {
        setPage();
        binding.btnCreateAPassword.setOnClickListener(this::onClick);

    }

    private void setPage(){
        keyPage = getActivity().getIntent().getIntExtra(Constants.KEYPAPE,0);
        switch (keyPage){
            case 0:
                binding.tvTitle.setText(getString(R.string.create_a_password));
                binding.btnCreateAPassword.setText(getString(R.string.create_a_password));
                break;
            case 1:
                binding.tvTitle.setText(getString(R.string.recover_pass));
                binding.btnCreateAPassword.setText(getString(R.string.recover_pass));
                break;
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_create_a_password:

                //TODO
                dialog = new DialogRegistrationConfirmation(getString(R.string.txt_dialog_register_successful),getString(R.string.login),false,new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getContext(), Login.class));
                        dialog.dismiss();
                    }
                });
                dialog.show(getChildFragmentManager(), "rer");


                break;
        }

    }
}
