package com.dinhlap.rohto.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.dinhlap.rohto.R;
import com.dinhlap.rohto.ui.fragment.HomeFagment;

import java.util.List;

public class BannerAdapter extends PagerAdapter{
    private Context context;
    private List<String> list;
    protected BannerListener bannerListener;


    LayoutInflater mLayoutInflater;

    public BannerAdapter(Context context, List<String> list, BannerListener bannerListener){
        this.context=context;
        this.list = list;
        this.bannerListener = bannerListener;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return list == null ? 0: list.size();
    }




    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.layout_banner, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_banner);
        Glide.with(context).load(list.get(position)).error(R.drawable.icon_banner).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bannerListener.onPhotoClick(list.get(position));
            }
        });

        container.addView(itemView);

        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView)object);
    }

    public  interface BannerListener{
        void  onPhotoClick(String path);
    }
}
