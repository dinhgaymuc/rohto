package com.dinhlap.rohto.base;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Errors implements Serializable {
   private int code;
   private String field;
   private String message;

   public String getMessage() {
      return message;
   }
}
