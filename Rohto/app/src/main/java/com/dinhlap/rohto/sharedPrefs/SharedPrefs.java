package com.dinhlap.rohto.sharedPrefs;

import android.content.Context;
import android.content.SharedPreferences;
import com.dinhlap.rohto.App;

public class SharedPrefs {

    //Cách dùng
//        SharedPrefs.getInstance().put(CURRENT_ID, 123);
//        SharedPrefs.getInstance().put(CURRENT_NAME, "NguyenDinhLap");
//
//        SharedPrefs.getInstance().get(CURRENT_ID, Integer.class);
//        SharedPrefs.getInstance().get(CURRENT_NAME, String.class);

    public static final String USER_NAME = "userName";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "accessToken";
    public static final String REFESHTOKEY = "refreshToken";

    private static SharedPrefs mInstance;
    private SharedPreferences mSharedPreferences;

    private SharedPrefs() {
        mSharedPreferences = App.self().getSharedPreferences("sharedPrefs_rohto", Context.MODE_PRIVATE);
    }

    public static SharedPrefs getInstance() {
        if (mInstance == null) {
            mInstance = new SharedPrefs();
        }
        return mInstance;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass) {
        if (anonymousClass == String.class) {
            return (T) String.valueOf(mSharedPreferences.getString(key, ""));
        } else if (anonymousClass == Boolean.class) {
            return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, false));
        } else if (anonymousClass == Float.class) {
            return (T) Float.valueOf(mSharedPreferences.getFloat(key, 0));
        } else if (anonymousClass == Integer.class) {
            return (T) Integer.valueOf(mSharedPreferences.getInt(key, 0));
        } else if (anonymousClass == Long.class) {
            return (T) Long.valueOf(mSharedPreferences.getLong(key, 0));
        } else {
            return (T) "null";
        }
    }

    public <T> void put(String key, T data) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        if (data instanceof String) {
            editor.putString(key, (String) data);
        } else if (data instanceof Boolean) {
            editor.putBoolean(key, (Boolean) data);
        } else if (data instanceof Float) {
            editor.putFloat(key, (Float) data);
        } else if (data instanceof Integer) {
            editor.putInt(key, (Integer) data);
        } else if (data instanceof Long) {
            editor.putLong(key, (Long) data);
        }
        editor.apply();
    }

//    public void clear() {mSharedPreferences.edit().clear().apply();}
}