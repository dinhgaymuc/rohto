package com.dinhlap.rohto.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.adapter.BannerAdapter;
import com.dinhlap.rohto.base.BaseFragment;
import com.dinhlap.rohto.base.CommonActivity;
import com.dinhlap.rohto.databinding.FragmentHomeBinding;
import com.dinhlap.rohto.utils.Constants;
import com.dinhlap.rohto.viewmodel.HomeVM;

import java.util.ArrayList;
import java.util.List;

import retrofit2.http.Header;

public class HomeFagment extends BaseFragment<FragmentHomeBinding, HomeVM> implements BannerAdapter.BannerListener {

    BannerAdapter bannerAdapter;
    List<String> list_banner;
    int p =0;

    public static HomeFagment getInstance() {
        return new HomeFagment();
    }
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected Class getViewModel() {
        return HomeVM.class;
    }

    @Override
    public void onCreateView() {

        setBannerViewPager();
        ImageView imageView = (ImageView) binding.include.findViewById(R.id.status_information);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, PersonalFagment.class);
                startActivity(intent);
            }
        });

    }

    private void setBannerViewPager(){
        list_banner = new ArrayList<>();
        list_banner.add("uhshdjsdhs");
        list_banner.add("uhshdjsdfhs");
        list_banner.add("uhshdjsdfhs");
        bannerAdapter = new BannerAdapter((Context) getContext(),list_banner,this::onPhotoClick);
        binding.viewPager.setAdapter(bannerAdapter);
        //binding.wormDotsIndicator.setViewPager(binding.viewPager);
        binding.tabLayout.setupWithViewPager(binding.viewPager, true);

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(p<2){
                    p++;
                }else {
                    p=0;
                }
                Log.d("lap",p+"");
                binding.viewPager.setCurrentItem(p);
                handler.postDelayed(this,5000);
            }
        },5000);

    }

    @Override
    public void onPhotoClick(String path) {
        Toast.makeText(getContext(),path, Toast.LENGTH_SHORT).show();

    }

}
