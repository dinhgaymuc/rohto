package com.dinhlap.rohto.ui.fragment;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.base.BaseFragment;
import com.dinhlap.rohto.databinding.FagmentPersonalBinding;
import com.dinhlap.rohto.viewmodel.PersonalVM;

public class PersonalFagment extends BaseFragment<FagmentPersonalBinding, PersonalVM> {
    @Override
    protected int getLayoutId() {
        return R.layout.fagment_personal;
    }

    @Override
    protected Class getViewModel() {
        return PersonalVM.class;
    }

    @Override
    public void onCreateView() {

    }
}
