package com.dinhlap.rohto.utils;

public class Constants {
    public static final String FRAGMENT = "FRAGMENT";
    public static final String KEYPAPE = "KEYPAPE";
    public static final int REGISTRATION = 0;
    public static final int FORGOTPASSWORD = 1;
}
