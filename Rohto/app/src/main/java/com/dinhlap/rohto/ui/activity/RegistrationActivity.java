package com.dinhlap.rohto.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.base.BaseActivity;
import com.dinhlap.rohto.base.CommonActivity;
import com.dinhlap.rohto.databinding.ActivityRegistrationBinding;
import com.dinhlap.rohto.dialog.DialogRegistrationConfirmation;
import com.dinhlap.rohto.model.Account;
import com.dinhlap.rohto.network.RunUi;
import com.dinhlap.rohto.ui.fragment.OtpFragment;
import com.dinhlap.rohto.utils.Constants;
import com.dinhlap.rohto.viewmodel.RegistrationVM;

public class RegistrationActivity extends BaseActivity<ActivityRegistrationBinding, RegistrationVM> implements View.OnClickListener, RunUi {
    DialogRegistrationConfirmation dialog;
    private int keyPage;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_registration;
    }

    @Override
    protected Class getViewModel() {
        return RegistrationVM.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.btnRegisterAccount.setOnClickListener(this::onClick);
        setKeyPage();

    }

    private void setKeyPage() {
        Intent intent = getIntent();
        keyPage = intent.getIntExtra(Constants.KEYPAPE, 0);
        switch (keyPage) {
            case 0:
                binding.tvTitle.setText(getString(R.string.register_an_account));
                binding.btnRegisterAccount.setText(getString(R.string.register));
                break;
            case 1:
                binding.tvTitle.setText(getString(R.string.forgot_password));
                binding.btnRegisterAccount.setText(getString(R.string.recover_pass));
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register_account:
                if (keyPage == 0) {

                } else if (keyPage == 1) {
                    if (Account.isEmailValid(binding.edtRegisterAccount.getText().toString().trim())) {
                        showDialog();
                    } else {
                    }

                }
                break;
        }
    }

    @Override
    public void runSuccess(Object... params) {

    }

    @Override
    public void runFailure(String error) {

    }

    private void showDialog() {
        String message = keyPage == 0 ? getString(R.string.successful_registration) : getString(R.string.successful_forgot_password);
        dialog = new DialogRegistrationConfirmation(message, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, OtpFragment.class);
                intent.putExtra(Constants.KEYPAPE, keyPage);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.show(getSupportFragmentManager(), "rer");
    }
}