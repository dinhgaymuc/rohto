package com.dinhlap.rohto.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.databinding.DialogRegistrationConfirmationBinding;

public class DialogRegistrationConfirmation  extends DialogFragment {

    private View.OnClickListener onClickListener;
    private String message;
    private String txtButton = null;
    private boolean type = true;

    public DialogRegistrationConfirmation(String message,View.OnClickListener onClickListener) {
        this.message = message;
        this.onClickListener = onClickListener;
    }

    public DialogRegistrationConfirmation(String message,String txtButton, Boolean type,View.OnClickListener onClickListener) {
        this.message = message;
        this.onClickListener = onClickListener;
        this.type=type;
        this.txtButton = txtButton;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogRegistrationConfirmationBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_registration_confirmation, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.custom_dialog_confirm));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        binding.tvMessage.setText(message);
        if(!type){
            binding.textView4.setVisibility(View.GONE);
        }
        if(txtButton!=null){
            binding.btnRegistrationConfirmation.setText(txtButton);
        }
        binding.btnRegistrationConfirmation.setOnClickListener(onClickListener);
        return binding.getRoot();
    }
}
