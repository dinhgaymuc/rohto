package com.dinhlap.rohto.viewmodel;
import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.dinhlap.rohto.base.ResultData;
import com.dinhlap.rohto.model.Account;
import com.dinhlap.rohto.model.Token;
import com.dinhlap.rohto.network.NetworkService;
import com.dinhlap.rohto.network.RunUi;
import com.dinhlap.rohto.sharedPrefs.SharedPrefs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginVM extends ViewModel {


    public void login(String userName, String password,  RunUi runUi){
        NetworkService.getService().login(new Account(userName,password)).enqueue(new Callback<ResultData<Token>>() {
            @Override
            public void onResponse(Call<ResultData<Token>> call, Response<ResultData<Token>> response) {
                if(response.isSuccessful() && response.body() !=null){
                    if(response.body().getData()!= null){
                        String token =response.body().getData().getAccessToken();
                        saveInfor(userName,password,token);
                        runUi.runSuccess(response.body().getData());
                    }else if(response.body().getErrors() != null){
                        runUi.runFailure("error");
                    }
                }else {
                    runUi.runFailure("Đăng nhập không thành công");
                }
            }

            @Override
            public void onFailure(Call<ResultData<Token>> call, Throwable t) {
                if(t != null){
                    runUi.runFailure(t.getMessage());
                }

            }
        });
    }

    private void saveInfor (String userName, String password, String token){
        SharedPrefs.getInstance().put(SharedPrefs.USER_NAME,userName);
        SharedPrefs.getInstance().put(SharedPrefs.PASSWORD,password);
        SharedPrefs.getInstance().put(SharedPrefs.TOKEN,token);
    }
}
