package com.dinhlap.rohto.base;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResultData<Data> implements Serializable {
    private String status;
    private Data data;
    private List<Errors> errors;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<Errors> getErrors() {
        return errors;
    }

}
