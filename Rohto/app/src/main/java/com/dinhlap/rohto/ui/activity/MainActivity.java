package com.dinhlap.rohto.ui.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.base.BaseActivity;
import com.dinhlap.rohto.databinding.ActivityMainBinding;
import com.dinhlap.rohto.ui.fragment.HomeFagment;
import com.dinhlap.rohto.viewmodel.MainVM;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainVM> implements BottomNavigationView.OnNavigationItemSelectedListener {


    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Class getViewModel() {
        return MainVM.class;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addList();
        binding.navigation.setOnNavigationItemSelectedListener(this);
        binding.navigation.setSelectedItemId(R.id.menu_home);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                addFragment(R.id.layout_FrameLayout, HomeFagment.getInstance(), null);
                hide(0);
                return true;
            case R.id.menu_skin_analysis:
                addFragment(R.id.layout_FrameLayout, HomeFagment.getInstance(), null);
                hide(1);
                return true;
            case R.id.menu_camera:
                addFragment(R.id.layout_FrameLayout, HomeFagment.getInstance(), null);
                hide(2);
                return true;
            case R.id.menu_grid:
                addFragment(R.id.layout_FrameLayout, HomeFagment.getInstance(), null);
                hide(3);
                return true;
            case R.id.menu_news:
                addFragment(R.id.layout_FrameLayout, HomeFagment.getInstance(), null);
                hide(4);
                return true;
            default:
                addFragment(R.id.layout_FrameLayout, HomeFagment.getInstance(), null);
                return true;
        }
    }

    List<RelativeLayout> list = new ArrayList<>();

    private void addList() {
        list.add(binding.home);
        list.add(binding.skin);
        list.add(binding.camera);
        list.add(binding.product);
        list.add(binding.news);
    }

    private void hide(int p) {

        for (int i = 0; i < 5; i++) {
            if (i == p) {
                list.get(i).setVisibility(View.VISIBLE);
            } else {
                list.get(i).setVisibility(View.INVISIBLE);
            }
        }

    }

}