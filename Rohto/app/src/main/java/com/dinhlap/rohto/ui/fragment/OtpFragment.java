package com.dinhlap.rohto.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.dinhlap.rohto.R;
import com.dinhlap.rohto.base.BaseFragment;
import com.dinhlap.rohto.base.CommonActivity;
import com.dinhlap.rohto.databinding.FragmentOtpBinding;
import com.dinhlap.rohto.utils.Constants;
import com.dinhlap.rohto.viewmodel.OtpVM;

public class OtpFragment extends BaseFragment<FragmentOtpBinding,OtpVM> implements View.OnClickListener {

    private int keyPage;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_otp;
    }

    @Override
    protected Class getViewModel() {
        return OtpVM.class;
    }


    @Override
    public void onCreateView() {
        setKeyPage();
        binding.btnRegisterAccount.setOnClickListener(this::onClick);

    }

    private  void  setKeyPage (){
        Intent intent = getActivity().getIntent();
        keyPage = intent.getIntExtra(Constants.KEYPAPE,0);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_register_account:
                Intent intent = new Intent(getContext(), CommonActivity.class);
                intent.putExtra(Constants.FRAGMENT, CreatePassFagment.class);
                intent.putExtra(Constants.KEYPAPE,keyPage);
                startActivity(intent);
                break;
        }

    }
}
